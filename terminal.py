import hashlib

accounts = {"john.doe":20,
            "xXwaltonchaingangXx":10,
            "cryptowojak123":1000,
            "not_b0gdan0ff":0,
            "sminem.1337":10,
            }

teller_balance = 0

flag = "WPI{duMp_33t_aNd_g@rn33sh_H1$_wAg3$}"

def hash_command(command):
    secret = "jdk123ipo32uijre"
    #bytes(secret+command, 'utf-8')
    hashed = hashlib.sha1(secret+command).hexdigest()
    
    token = hashed
    return token

def validate_input(command, token_in):
    token = hash_command(command)

    if token == token_in:
        return True
    else:
        return False

def execute_command(command):
    global accounts
    global teller_balance

    subs = command.split(";")
    for sub in subs:
        components = sub.split(" ")
        action, target = components[0], components[1]

        if action == "withdraw":
            try:
                teller_balance += accounts[target]
                accounts[target] = 0
                print("Action successful!")
            except KeyError:
                print("A subcommand was unreadable...")
        elif action == "deposit":
            try:
                accounts[target] += teller_balance
                teller_balance = 0
                print("Action successful!")
            except KeyError:
                print("A subcommand was unreadable...")
        else:
            print("A subcommand was unreadable...")

def print_terminal():
    print("Welcome to the Binance Teller Terminal!")
    print("Please remember to use admin-issued auth tokens with each account transfer!")
    print
    print("Either enter a command or one of the following keywords:")
    print
    print("accounts: List of accounts currently on the system.")
    print("history: A history of prior terminal commands.")
    print("help: A reminder on how to use this terminal.")
    print


def print_accounts():
    print
    print("Accounts:")
    print("john.doe")
    print("not_b0gdan0ff")
    print("cryptowojak123")
    print("xXwaltonchaingangXx")
    print("sminem.1337")
    print

def print_file(path):
    print
    with open(path, 'r+') as f:
        print(f.read())

def print_help():
    print
    print("You may either withdraw funds from an account or deposit funds to an account.")
    print("Withdraw with the following command:")
    print("withdraw ACCOUNT_NAME")
    print("Deposit with the following command:")
    print("deposit ACCOUNT_NAME")
    print("Commands may be chained, as follows:")
    print("withdraw ACCOUNT_NAME;deposit ACCOUNT_NAME;...")
    print
    print("An authorization token unique to the command contents must exist for the transaction to succeed!")
    print("(Sorry, but we have to protect from malicious employees.)")
    print("Contact admin@dontactuallyemailthis.net to get auth tokens for different transfer commands!")
    print

print_file("intro.txt")
print("\n" * 7)


print_terminal()
while(True):
    print("Command:")
    command = raw_input(">>>")
    #command = bytes(command, 'utf-8')
    if command == "accounts":
        print_accounts()
    elif command == "history":
        print_file("history.txt")
    elif command == "help":
        print_help()
    elif command == "":
        print
        pass
    else: 
        print('Auth token:')
        token = raw_input(">>>")
        print
        if validate_input(command, token) == False:
            print("Error: Auth token does not match provided command..")
        else:
            execute_command(command)
        print

    if accounts["not_b0gdan0ff"] >= 1000 and accounts["cryptowojak123"] == 0 :
        print_file("victory.txt")
        print(flag)
        exit()










        
