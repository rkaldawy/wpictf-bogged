import hashpumpy as pump


#we dont know secret size, so lets cycle through potential values

for i in range(0, 100):
    digest, message = pump.hashpump("b4c967e157fad98060ebbf24135bfdb5a73f14dc",
                                    "withdraw john.doe", 
                                    ";withdraw cryptowojak123;deposit not_b0gdan0ff",
                                    i)

    #print(i, digest, message)
    #print('\n'*2)
    #in pwntools you could just write this to try them one by one

digest, message = pump.hashpump("b4c967e157fad98060ebbf24135bfdb5a73f14dc",
                                    "withdraw john.doe", 
                                    ";withdraw cryptowojak123;deposit not_b0gdan0ff",
                                    16)

#print("".join( chr(x) for x in message))
#print(message)
#print(digest)
with open("solution.txt", "wb") as f:
    f.write(message)
    f.write(bytes('\n', 'utf-8'))
    f.write(bytes(digest, 'utf-8'))
    
